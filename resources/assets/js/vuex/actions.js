export const actions = {
    _TEST_ACTION( {state, commit, dispatch}, data ) {
        console.log('This is a test Vuex Action')
    },
    _TEST_ACTION_GETTER( { getters } ) {
        console.log( getters._testGetter + ' invoked using Vuex Action' )
    }
}
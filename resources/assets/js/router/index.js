import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../components/pages/Home.vue'
import Services from '../components/pages/Services.vue'
import Contact from '../components/pages/Contact.vue'

Vue.use(VueRouter)

export default new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Index',
            component: Home
        },
        {
            path: '/home',
            name: 'Home',
            component: Home
        },
        {
            path: '/services',
            name: 'Services',
            component: Services
        },
        {
            path: '/contact',
            name: 'Contact',
            component: Contact
        }
    ]
})
require('./bootstrap');
window.Vue = require('vue')

import router from './router'
import store from './vuex'
require('./components')

const app = new Vue({
    el: '#app',
    router,
    store
})

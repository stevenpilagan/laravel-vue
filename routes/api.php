<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::prefix('/v1/user')->group(function () {
    Route::get('/register', 'UserController@register');
    Route::get('/retrieve', 'UserController@retrieve');
    Route::get('/update', 'UserController@update');
    Route::get('/remove', 'UserController@remove');
});



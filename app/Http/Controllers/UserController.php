<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateUser;
use App\User;

class UserController extends Controller
{
    public function register( CreateUser $request ) {
        if( $request ) {
            $request['password'] = bcrypt( $request['password'] );
            return User::register( $request->all() );
        }
    }

    public function retrieve() {
        return 'retrieve';
    }

    public function update() {
        return 'update';
    }

    public function remove() {
        return 'delete';
    }
}

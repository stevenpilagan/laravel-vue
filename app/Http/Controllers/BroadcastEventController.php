<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BroadcastEventController extends Controller
{
    public function eventUserRegistered() {
        return 'eventUserRegistered';
    }

    public function eventUserUpdated() {
        return 'eventUserUpdated';
    }

    public function eventUserRemoved() {
        return 'eventUserRemoved';
    }
}

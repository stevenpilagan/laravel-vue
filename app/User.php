<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Events\User\Registered;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function register( $data ) {
        $user = User::create( $data );
        
        if( $user ) {

            event( new Registered( $user ) );

            return [
                'status' => 'ok',
                'message' => 'User has been added'
            ];
        }
    }

    public static function retrieve() {
        return 'retrieve';
    }

    public static function revise() {
        return 'update';
    }

    public static function remove() {
        return 'delete';
    }
}
